const gameContainer = document.querySelector('.game');

function createGameBoard() {
    for (let i = 0; i < 25; i++) {
        const gameItem = document.createElement('li');
        gameItem.classList.add('game-item');
        gameContainer.appendChild(gameItem);
    }
}

createGameBoard();